-- *************** SqlDBM: PostgreSQL ****************;
-- ***************************************************;

DROP TABLE IF EXISTS "public"."customer";


DROP TABLE IF EXISTS "public"."address";



-- ************************************** "public"."address"

CREATE TABLE IF NOT EXISTS "public"."address"
(
 "address_id"    smallserial NOT NULL,
 "address_line1" varchar(50) NOT NULL,
 "address_line2" varchar(50) NOT NULL,
 "city"          varchar(50) NOT NULL,
 "country"       varchar(50) NOT NULL,
 "state"         varchar(50) NOT NULL,
 "zip"           varchar(50) NOT NULL

);

CREATE UNIQUE INDEX "PK_address" ON "public"."address"
(
 "address_id"
);

-- *************** SqlDBM: PostgreSQL ****************;
-- ***************************************************;

DROP TABLE IF EXISTS "public"."address";



-- ************************************** "public"."address"

CREATE TABLE IF NOT EXISTS "public"."address"
(
 "address_id"    smallserial NOT NULL,
 "address_line1" varchar(50) NOT NULL,
 "address_line2" varchar(50) NOT NULL,
 "city"          varchar(50) NOT NULL,
 "country"       varchar(50) NOT NULL,
 "state"         varchar(50) NOT NULL,
 "zip"           varchar(50) NOT NULL

);

CREATE UNIQUE INDEX "PK_address" ON "public"."address"
(
 "address_id"
);

CREATE UNIQUE INDEX "Uq_Address" ON "public"."address"
(
 "address_line1",
 "address_line2",
 "city",
 "country",
 "state",
 "zip"
);









-- ************************************** "public"."customer"

CREATE TABLE IF NOT EXISTS "public"."customer"
(
 "customer_id" smallserial NOT NULL,
 "first_name"  varchar(50) NOT NULL,
 "last_name"   varchar(50) NOT NULL,
 "email"       varchar(50) NOT NULL,
 "phone"       varchar(50) NOT NULL,
 "address_id"  smallserial NOT NULL,
 CONSTRAINT "FK_37" FOREIGN KEY ( "address_id" ) REFERENCES "public"."address" ( "address_id" )
);

CREATE UNIQUE INDEX "PK_Customer" ON "public"."customer"
(
 "customer_id"
);

CREATE INDEX "fkIdx_37" ON "public"."customer"
(
 "address_id"
);

CREATE INDEX "Uq_email_phone" ON "public"."customer"
(
 "customer_id",
 "email",
 "phone"
);







-- *************** SqlDBM: PostgreSQL ****************;
-- ***************************************************;

DROP TABLE IF EXISTS "public"."customer_mail";


DROP TABLE IF EXISTS "public"."employee";



-- ************************************** "public"."employee"

CREATE TABLE IF NOT EXISTS "public"."employee"
(
 "employee_id" smallserial NOT NULL,
 "first_name"  varchar(50) NOT NULL,
 "last_name"   varchar(50) NOT NULL,
 "email"       varchar(50) NOT NULL,
 "phone"       varchar(50) NOT NULL,
 "role"        varchar(50) NOT NULL,
 "address_id"  smallserial NOT NULL,
 CONSTRAINT "FK_116" FOREIGN KEY ( "address_id" ) REFERENCES "public"."address" ( "address_id" )
);

CREATE UNIQUE INDEX "PK_employee" ON "public"."employee"
(
 "employee_id"
);

CREATE INDEX "fkIdx_116" ON "public"."employee"
(
 "address_id"
);








-- ************************************** "public"."customer_mail"

CREATE TABLE IF NOT EXISTS "public"."customer_mail"
(
 "contact_id"   smallserial NOT NULL,
 "customer_id"  smallserial NOT NULL,
 "contact_type" varchar(50) NOT NULL,
 "contact_body" varchar(50) NOT NULL,
 "contact_info" varchar(50) NOT NULL,
 "status"       varchar(50) NOT NULL,
 CONSTRAINT "FK_84" FOREIGN KEY ( "customer_id" ) REFERENCES "public"."customer" ( "customer_id" )
);

CREATE UNIQUE INDEX "PK_customer_mail" ON "public"."customer_mail"
(
 "contact_id",
 "customer_id"
);

CREATE INDEX "fkIdx_84" ON "public"."customer_mail"
(
 "customer_id"
);







-- *************** SqlDBM: PostgreSQL ****************;
-- ***************************************************;

DROP TABLE IF EXISTS "public"."service_request";


DROP TABLE IF EXISTS "public"."vehicle";



-- ************************************** "public"."vehicle"

CREATE TABLE IF NOT EXISTS "public"."vehicle"
(
 "vehicle_reg_no"        varchar(50) NOT NULL,
 "vehicle_id"            smallserial NOT NULL,
 "vehicle_make"          varchar(50) NOT NULL,
 "vehicle_purchase_date" date NOT NULL,
 "has_insurance"         boolean NOT NULL,
 "customer_id"           smallserial NOT NULL,
 CONSTRAINT "FK_48" FOREIGN KEY ( "customer_id" ) REFERENCES "public"."customer" ( "customer_id" )
);

CREATE UNIQUE INDEX "PK_vehicle" ON "public"."vehicle"
(
 "vehicle_id"
);

CREATE INDEX "fkIdx_48" ON "public"."vehicle"
(
 "customer_id"
);








-- ************************************** "public"."service_request"

CREATE TABLE IF NOT EXISTS "public"."service_request"
(
 "service_id"  smallserial NOT NULL,
 "start_date"  date NOT NULL,
 "end_date"    date NOT NULL,
 "status"      varchar(50) NOT NULL,
 "vehicle_id"  smallserial NOT NULL,
 "employee_id" smallserial NOT NULL,
 CONSTRAINT "FK_119" FOREIGN KEY ( "employee_id" ) REFERENCES "public"."employee" ( "employee_id" ),
 CONSTRAINT "FK_65" FOREIGN KEY ( "vehicle_id" ) REFERENCES "public"."vehicle" ( "vehicle_id" )
);

CREATE UNIQUE INDEX "PK_service" ON "public"."service_request"
(
 "service_id"
);

CREATE INDEX "fkIdx_119" ON "public"."service_request"
(
 "employee_id"
);

CREATE INDEX "fkIdx_65" ON "public"."service_request"
(
 "vehicle_id"
);







