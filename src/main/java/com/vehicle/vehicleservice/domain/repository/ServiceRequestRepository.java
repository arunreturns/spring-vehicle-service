package com.vehicle.vehicleservice.domain.repository;

import java.util.List;

import com.vehicle.vehicleservice.domain.model.ServiceRequest;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ServiceRequestRepository extends CrudRepository<ServiceRequest, Long> {
  @Override
  List<ServiceRequest> findAll();
}