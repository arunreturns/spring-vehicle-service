package com.vehicle.vehicleservice.domain.repository;

import java.util.List;

import com.vehicle.vehicleservice.domain.model.CustomerMail;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerMailRepository extends CrudRepository<CustomerMail, Long> {
  @Override
  List<CustomerMail> findAll();
}