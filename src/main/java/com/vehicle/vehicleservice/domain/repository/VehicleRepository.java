package com.vehicle.vehicleservice.domain.repository;

import java.util.List;

import com.vehicle.vehicleservice.domain.model.Vehicle;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VehicleRepository extends CrudRepository<Vehicle, Long> {
  @Override
  List<Vehicle> findAll();
}