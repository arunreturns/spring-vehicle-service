package com.vehicle.vehicleservice.domain.repository;

import java.util.List;

import com.vehicle.vehicleservice.domain.model.Address;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends CrudRepository<Address, Long> {
  @Override
  List<Address> findAll();
}