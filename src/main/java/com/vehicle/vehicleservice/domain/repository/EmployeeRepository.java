package com.vehicle.vehicleservice.domain.repository;

import java.util.List;

import com.vehicle.vehicleservice.domain.model.Employee;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Long> {
  @Override
  List<Employee> findAll();
}