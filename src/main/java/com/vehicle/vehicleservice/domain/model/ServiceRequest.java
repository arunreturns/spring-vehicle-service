package com.vehicle.vehicleservice.domain.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode
@Entity
public class ServiceRequest implements Serializable {
  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long serviceId;
  private Date startDate;
  private Date endDate;
  private String status;
  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "vehicle_id", insertable = true, updatable = true)
  private Vehicle vehicle;
  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "employee_id", insertable = true, updatable = true)
  private Employee employee;
}