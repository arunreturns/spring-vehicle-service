package com.vehicle.vehicleservice.domain.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode
@Entity
public class Vehicle implements Serializable {
  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long vehicleId;
  private String vehicleRegNo;
  private String vehicleMake;
  private Date vehiclePurchaseDate;
  private Boolean hasInsurance;
  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "customer_id", insertable = true, updatable = true)
  private Customer customer;
}