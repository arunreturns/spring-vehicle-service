package com.vehicle.vehicleservice.api.v1;

import java.util.List;

import javax.validation.Valid;

import com.vehicle.vehicleservice.api.request.AddCustomerMail;
import com.vehicle.vehicleservice.domain.repository.CustomerMailRepository;
import com.vehicle.vehicleservice.dto.CustomerMailDto;
import com.vehicle.vehicleservice.mapper.CustomerMailMapper;
import com.vehicle.vehicleservice.service.CustomerMailService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customerMail")
public class CustomerMailController {
  private CustomerMailRepository customerMailRepository;
  private CustomerMailService customerMailService;

  @Autowired
  public CustomerMailController(CustomerMailRepository customerMailRepository,
      CustomerMailService customerMailService) {
    this.customerMailRepository = customerMailRepository;
    this.customerMailService = customerMailService;
  }

  @GetMapping("/")
  public List<CustomerMailDto> getCustomerMails() {
    return CustomerMailMapper.mapCustomerMails(customerMailRepository.findAll());
  }

  @PostMapping(value = "/")
  public CustomerMailDto addCustomerMail(@RequestBody @Valid AddCustomerMail addCustomerMail) {
    return customerMailService.addCustomerMail(addCustomerMail);
  }

  @DeleteMapping(value = "/{customerMailId}")
  public void deleteCustomerMail(@PathVariable Long customerMailId) {
    customerMailRepository.deleteById(customerMailId);
  }
}