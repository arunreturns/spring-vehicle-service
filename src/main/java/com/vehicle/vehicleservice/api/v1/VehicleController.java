package com.vehicle.vehicleservice.api.v1;

import java.util.List;

import javax.validation.Valid;

import com.vehicle.vehicleservice.api.request.AddVehicle;
import com.vehicle.vehicleservice.domain.repository.VehicleRepository;
import com.vehicle.vehicleservice.dto.VehicleDto;
import com.vehicle.vehicleservice.mapper.VehicleMapper;
import com.vehicle.vehicleservice.service.VehicleService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/vehicle")
public class VehicleController {
  private VehicleRepository vehicleRepository;
  private VehicleService vehicleService;

  @Autowired
  public VehicleController(VehicleRepository vehicleRepository, VehicleService vehicleService) {
    this.vehicleRepository = vehicleRepository;
    this.vehicleService = vehicleService;
  }

  @GetMapping("/")
  public List<VehicleDto> getVehicles() {
    return VehicleMapper.mapVehicles(vehicleRepository.findAll());
  }

  @PostMapping(value = "/")
  public VehicleDto addVehicle(@RequestBody @Valid AddVehicle addVehicle) {
    return vehicleService.addVehicle(addVehicle);
  }

  @DeleteMapping(value = "/{vehicleId}")
  public void deleteVehicle(@PathVariable Long vehicleId) {
    vehicleRepository.deleteById(vehicleId);
  }
}