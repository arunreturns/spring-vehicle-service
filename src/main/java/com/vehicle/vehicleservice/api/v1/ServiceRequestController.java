package com.vehicle.vehicleservice.api.v1;

import java.util.List;

import javax.validation.Valid;

import com.vehicle.vehicleservice.api.request.AddServiceRequest;
import com.vehicle.vehicleservice.domain.repository.ServiceRequestRepository;
import com.vehicle.vehicleservice.dto.ServiceRequestDto;
import com.vehicle.vehicleservice.mapper.ServiceRequestMapper;
import com.vehicle.vehicleservice.service.ServiceRequestService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/serviceRequest")
public class ServiceRequestController {
  private ServiceRequestRepository serviceRequestRepository;
  private ServiceRequestService serviceRequestService;

  @Autowired
  public ServiceRequestController(ServiceRequestRepository serviceRequestRepository,
      ServiceRequestService serviceRequestService) {
    this.serviceRequestRepository = serviceRequestRepository;
    this.serviceRequestService = serviceRequestService;
  }

  @GetMapping("/")
  public List<ServiceRequestDto> getServiceRequests() {
    return ServiceRequestMapper.mapServiceRequests(serviceRequestRepository.findAll());
  }

  @PostMapping(value = "/")
  public ServiceRequestDto addServiceRequest(@RequestBody @Valid AddServiceRequest addServiceRequest) {
    return serviceRequestService.addServiceRequest(addServiceRequest);
  }

  @DeleteMapping(value = "/{serviceRequestId}")
  public void deleteServiceRequest(@PathVariable Long serviceRequestId) {
    serviceRequestRepository.deleteById(serviceRequestId);
  }
}