package com.vehicle.vehicleservice.api.v1;

import java.util.List;

import javax.validation.Valid;

import com.vehicle.vehicleservice.api.request.AddEmployee;
import com.vehicle.vehicleservice.domain.repository.EmployeeRepository;
import com.vehicle.vehicleservice.dto.EmployeeDto;
import com.vehicle.vehicleservice.mapper.EmployeeMapper;
import com.vehicle.vehicleservice.service.EmployeeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/employee")
public class EmployeeController {
  private EmployeeRepository employeeRepository;
  private EmployeeService employeeService;

  @Autowired
  public EmployeeController(EmployeeRepository employeeRepository, EmployeeService employeeService) {
    this.employeeRepository = employeeRepository;
    this.employeeService = employeeService;
  }

  @GetMapping("/")
  public List<EmployeeDto> getEmployees() {
    return EmployeeMapper.mapEmployees(employeeRepository.findAll());
  }

  @PostMapping(value = "/")
  public EmployeeDto addEmployee(@RequestBody @Valid AddEmployee addEmployee) {
    return employeeService.addEmployee(addEmployee);
  }

  @DeleteMapping(value = "/{employeeId}")
  public void deleteEmployee(@PathVariable Long employeeId) {
    employeeRepository.deleteById(employeeId);
  }
}