package com.vehicle.vehicleservice.api.v1;

import java.util.List;

import javax.validation.Valid;

import com.vehicle.vehicleservice.api.request.AddCustomer;
import com.vehicle.vehicleservice.domain.repository.CustomerRepository;
import com.vehicle.vehicleservice.dto.CustomerDto;
import com.vehicle.vehicleservice.mapper.CustomerMapper;
import com.vehicle.vehicleservice.service.CustomerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customer")
public class CustomerController {
  private CustomerRepository customerRepository;
  private CustomerService customerService;

  @Autowired
  public CustomerController(CustomerRepository customerRepository, CustomerService customerService) {
    this.customerRepository = customerRepository;
    this.customerService = customerService;
  }

  @GetMapping(value = "/")
  public List<CustomerDto> getCustomers() {
    return CustomerMapper.mapCustomers(customerRepository.findAll());
  }

  @PostMapping(value = "/")
  public CustomerDto addCustomer(@RequestBody @Valid AddCustomer addCustomer) {
    return customerService.addCustomer(addCustomer);
  }

  @DeleteMapping(value = "/{customerId}")
  public void deleteCustomer(@PathVariable Long customerId) {
    customerRepository.deleteById(customerId);
  }
}