package com.vehicle.vehicleservice.api.v1;

import java.util.List;

import javax.validation.Valid;

import com.vehicle.vehicleservice.api.request.AddAddress;
import com.vehicle.vehicleservice.domain.repository.AddressRepository;
import com.vehicle.vehicleservice.dto.AddressDto;
import com.vehicle.vehicleservice.mapper.AddressMapper;
import com.vehicle.vehicleservice.service.AddressService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/address")
public class AddressController {
  private AddressRepository addressRepository;
  private AddressService addressService;

  @Autowired
  public AddressController(AddressRepository addressRepository, AddressService addressService) {
    this.addressRepository = addressRepository;
    this.addressService = addressService;
  }

  @GetMapping(value = "/")
  public List<AddressDto> getAddresss() {
    return AddressMapper.mapAddresss(addressRepository.findAll());
  }

  @PostMapping(value = "/")
  public AddressDto addAddress(@RequestBody @Valid AddAddress addAddress) {
    return addressService.addAddress(addAddress);
  }

  @DeleteMapping(value = "/{addressId}")
  public void deleteAddress(@PathVariable Long addressId) {
    addressRepository.deleteById(addressId);
  }
}