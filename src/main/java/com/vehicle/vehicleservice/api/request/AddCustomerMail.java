package com.vehicle.vehicleservice.api.request;

import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode
public class AddCustomerMail {
  @NotEmpty(message = "contactType is required")
  private String contactType;
  @NotEmpty(message = "contactBody is required")
  private String contactBody;
  @NotEmpty(message = "contactInfo is required")
  private String contactInfo;
  @NotEmpty(message = "status is required")
  private String status;
  private Long customerId;
}