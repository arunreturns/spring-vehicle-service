package com.vehicle.vehicleservice.api.request;

import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode
public class AddCustomer {
  @NotEmpty(message = "firstName is required")
  private String firstName;
  @NotEmpty(message = "lastName is required")
  private String lastName;
  @NotEmpty(message = "email is required")
  private String email;
  @NotEmpty(message = "phone is required")
  private String phone;
  private Long addressId;
}