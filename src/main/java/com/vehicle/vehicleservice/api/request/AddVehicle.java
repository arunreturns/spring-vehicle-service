package com.vehicle.vehicleservice.api.request;

import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Past;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode
public class AddVehicle {
  @NotEmpty(message = "vehicleRegNo is required")
  private String vehicleRegNo;
  @NotEmpty(message = "vehicleMake is required")
  private String vehicleMake;
  @Past(message = "Purchase date must be valid")
  private Date vehiclePurchaseDate;
  private Boolean hasInsurance;
  private Long customerId;
}