package com.vehicle.vehicleservice.api.request;

import java.util.Date;

import javax.validation.constraints.Future;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode
public class AddServiceRequest {
  @FutureOrPresent(message = "startDate should be valid")
  private Date startDate;
  @Future(message = "endDate should be valid")
  private Date endDate;
  @NotEmpty(message = "status is required")
  private String status;
  private Long employeeId;
  private Long vehicleId;
}