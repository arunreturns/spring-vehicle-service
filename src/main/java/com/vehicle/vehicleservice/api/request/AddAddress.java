package com.vehicle.vehicleservice.api.request;

import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode
public class AddAddress {
  @NotEmpty(message = "addressLine1 is required")
  private String addressLine1;
  private String addressLine2;
  @NotEmpty(message = "city is required")
  private String city;
  @NotEmpty(message = "country is required")
  private String country;
  @NotEmpty(message = "state is required")
  private String state;
  @NotEmpty(message = "zip is required")
  private String zip;
}