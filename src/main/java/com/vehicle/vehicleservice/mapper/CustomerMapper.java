package com.vehicle.vehicleservice.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.vehicle.vehicleservice.domain.model.Customer;
import com.vehicle.vehicleservice.dto.CustomerDto;

public class CustomerMapper {
  public static List<CustomerDto> mapCustomers(List<Customer> customers) {
    return customers.stream().map(CustomerMapper::mapCustomer).collect(Collectors.toList());
  }

  public static CustomerDto mapCustomer(Customer customer) {
    CustomerDto customerDto = new CustomerDto();

    customerDto.setCustomerId(customer.getCustomerId());
    customerDto.setEmail(customer.getEmail());
    customerDto.setPhone(customer.getPhone());
    customerDto.setFirstName(customer.getFirstName());
    customerDto.setLastName(customer.getLastName());
    customerDto.setAddress(AddressMapper.mapAddress(customer.getAddress()));

    return customerDto;
  }
}