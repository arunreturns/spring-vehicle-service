package com.vehicle.vehicleservice.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.vehicle.vehicleservice.domain.model.CustomerMail;
import com.vehicle.vehicleservice.dto.CustomerMailDto;

public class CustomerMailMapper {
  public static List<CustomerMailDto> mapCustomerMails(List<CustomerMail> customerMails) {
    return customerMails.stream().map(CustomerMailMapper::mapCustomerMail).collect(Collectors.toList());
  }

  public static CustomerMailDto mapCustomerMail(CustomerMail customerMail) {
    CustomerMailDto customerMailDto = new CustomerMailDto();

    customerMailDto.setContactId(customerMail.getContactId());
    customerMailDto.setContactType(customerMail.getContactType());
    customerMailDto.setContactInfo(customerMail.getContactInfo());
    customerMailDto.setContactBody(customerMail.getContactBody());
    customerMailDto.setStatus(customerMail.getStatus());
    customerMailDto.setCustomer(CustomerMapper.mapCustomer(customerMail.getCustomer()));

    return customerMailDto;
  }
}