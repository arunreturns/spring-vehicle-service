package com.vehicle.vehicleservice.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.vehicle.vehicleservice.domain.model.ServiceRequest;
import com.vehicle.vehicleservice.dto.ServiceRequestDto;

public class ServiceRequestMapper {
  public static List<ServiceRequestDto> mapServiceRequests(List<ServiceRequest> serviceRequests) {
    return serviceRequests.stream().map(ServiceRequestMapper::mapServiceRequest).collect(Collectors.toList());
  }

  public static ServiceRequestDto mapServiceRequest(ServiceRequest serviceRequest) {
    ServiceRequestDto serviceRequestDto = new ServiceRequestDto();

    serviceRequestDto.setServiceId(serviceRequest.getServiceId());
    serviceRequestDto.setStartDate(serviceRequest.getStartDate());
    serviceRequestDto.setEndDate(serviceRequest.getEndDate());
    serviceRequestDto.setStatus(serviceRequest.getStatus());
    serviceRequestDto.setEmployee(EmployeeMapper.mapEmployee(serviceRequest.getEmployee()));
    serviceRequestDto.setVehicle(VehicleMapper.mapVehicle(serviceRequest.getVehicle()));

    return serviceRequestDto;
  }
}