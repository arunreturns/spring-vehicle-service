package com.vehicle.vehicleservice.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.vehicle.vehicleservice.domain.model.Address;
import com.vehicle.vehicleservice.dto.AddressDto;

public class AddressMapper {
  public static List<AddressDto> mapAddresss(List<Address> addresses) {
    return addresses.stream().map(AddressMapper::mapAddress).collect(Collectors.toList());
  }

  public static AddressDto mapAddress(Address address) {
    AddressDto addressDto = new AddressDto();

    addressDto.setAddressId(address.getAddressId());
    addressDto.setAddressLine1(address.getAddressLine1());
    addressDto.setAddressLine2(address.getAddressLine2());
    addressDto.setCity(address.getCity());
    addressDto.setCountry(address.getCountry());
    addressDto.setState(address.getState());
    addressDto.setZip(address.getZip());

    return addressDto;
  }
}