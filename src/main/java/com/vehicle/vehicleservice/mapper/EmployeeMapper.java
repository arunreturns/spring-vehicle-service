package com.vehicle.vehicleservice.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.vehicle.vehicleservice.domain.model.Employee;
import com.vehicle.vehicleservice.dto.EmployeeDto;

public class EmployeeMapper {
  public static List<EmployeeDto> mapEmployees(List<Employee> employees) {
    return employees.stream().map(EmployeeMapper::mapEmployee).collect(Collectors.toList());
  }

  public static EmployeeDto mapEmployee(Employee employee) {
    EmployeeDto employeeDto = new EmployeeDto();

    employeeDto.setEmployeeId(employee.getEmployeeId());
    employeeDto.setEmail(employee.getEmail());
    employeeDto.setPhone(employee.getPhone());
    employeeDto.setFirstName(employee.getFirstName());
    employeeDto.setLastName(employee.getLastName());
    employeeDto.setRole(employee.getRole());
    employeeDto.setAddress(AddressMapper.mapAddress(employee.getAddress()));

    return employeeDto;
  }
}