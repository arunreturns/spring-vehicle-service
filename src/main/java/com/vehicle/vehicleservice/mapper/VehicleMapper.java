package com.vehicle.vehicleservice.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.vehicle.vehicleservice.domain.model.Vehicle;
import com.vehicle.vehicleservice.dto.VehicleDto;

public class VehicleMapper {
  public static List<VehicleDto> mapVehicles(List<Vehicle> vehicles) {
    return vehicles.stream().map(VehicleMapper::mapVehicle).collect(Collectors.toList());
  }

  public static VehicleDto mapVehicle(Vehicle vehicle) {
    VehicleDto vehicleDto = new VehicleDto();

    vehicleDto.setVehicleId(vehicle.getVehicleId());
    vehicleDto.setVehicleRegNo(vehicle.getVehicleRegNo());
    vehicleDto.setVehicleMake(vehicle.getVehicleMake());
    vehicleDto.setVehiclePurchaseDate(vehicle.getVehiclePurchaseDate());
    vehicleDto.setHasInsurance(vehicle.getHasInsurance());
    vehicleDto.setCustomer(CustomerMapper.mapCustomer(vehicle.getCustomer()));

    return vehicleDto;
  }
}