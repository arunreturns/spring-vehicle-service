package com.vehicle.vehicleservice.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode
public class ServiceRequestDto {
  private Long serviceId;
  private Date startDate;
  private Date endDate;
  private String status;
  private VehicleDto vehicle;
  private EmployeeDto employee;
}