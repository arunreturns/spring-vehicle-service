package com.vehicle.vehicleservice.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode
public class CustomerMailDto {
  private Long contactId;
  private CustomerDto customer;
  private String contactType;
  private String contactBody;
  private String contactInfo;
  private String status;
}