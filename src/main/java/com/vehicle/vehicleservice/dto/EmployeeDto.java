package com.vehicle.vehicleservice.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode
public class EmployeeDto {
  private Long employeeId;
  private String firstName;
  private String lastName;
  private String email;
  private String phone;
  private String role;
  private AddressDto address;
}