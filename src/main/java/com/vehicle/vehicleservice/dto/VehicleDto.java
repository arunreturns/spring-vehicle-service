package com.vehicle.vehicleservice.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode
public class VehicleDto {
  private Long vehicleId;
  private String vehicleRegNo;
  private String vehicleMake;
  private Date vehiclePurchaseDate;
  private Boolean hasInsurance;
  private CustomerDto customer;
}