package com.vehicle.vehicleservice.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode
public class AddressDto {
  private Long addressId;
  private String addressLine1;
  private String addressLine2;
  private String city;
  private String country;
  private String state;
  private String zip;
}