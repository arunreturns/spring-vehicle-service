package com.vehicle.vehicleservice.service;

import java.util.Optional;

import com.vehicle.vehicleservice.api.request.AddServiceRequest;
import com.vehicle.vehicleservice.domain.model.Employee;
import com.vehicle.vehicleservice.domain.model.ServiceRequest;
import com.vehicle.vehicleservice.domain.model.Vehicle;
import com.vehicle.vehicleservice.domain.repository.EmployeeRepository;
import com.vehicle.vehicleservice.domain.repository.ServiceRequestRepository;
import com.vehicle.vehicleservice.domain.repository.VehicleRepository;
import com.vehicle.vehicleservice.dto.ServiceRequestDto;
import com.vehicle.vehicleservice.mapper.ServiceRequestMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceRequestService {
  @Autowired
  private ServiceRequestRepository serviceRequestRepository;
  @Autowired
  private EmployeeRepository employeeRepository;
  @Autowired
  private VehicleRepository vehicleRepository;

  public ServiceRequestDto addServiceRequest(AddServiceRequest addServiceRequest) {
    ServiceRequest serviceRequest = new ServiceRequest();

    Optional<Employee> employee = employeeRepository.findById(addServiceRequest.getEmployeeId());
    if (employee.isPresent()) {
      serviceRequest.setEmployee(employee.get());
    } else {
      return null;
    }

    Optional<Vehicle> vehicle = vehicleRepository.findById(addServiceRequest.getVehicleId());
    if (vehicle.isPresent()) {
      serviceRequest.setVehicle(vehicle.get());
    } else {
      return null;
    }

    serviceRequest.setStartDate(addServiceRequest.getStartDate());
    serviceRequest.setEndDate(addServiceRequest.getEndDate());
    serviceRequest.setStatus(addServiceRequest.getStatus());

    return ServiceRequestMapper.mapServiceRequest(serviceRequestRepository.save(serviceRequest));
  }
}