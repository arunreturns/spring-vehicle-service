package com.vehicle.vehicleservice.service;

import java.util.Optional;

import com.vehicle.vehicleservice.api.request.AddCustomer;
import com.vehicle.vehicleservice.domain.model.Address;
import com.vehicle.vehicleservice.domain.model.Customer;
import com.vehicle.vehicleservice.domain.repository.AddressRepository;
import com.vehicle.vehicleservice.domain.repository.CustomerRepository;
import com.vehicle.vehicleservice.dto.CustomerDto;
import com.vehicle.vehicleservice.mapper.CustomerMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {
  private final Logger logger = LoggerFactory.getLogger(this.getClass());
  @Autowired
  private CustomerRepository customerRepository;
  @Autowired
  private AddressRepository addressRepository;

  public CustomerDto addCustomer(AddCustomer addCustomer) {
    Customer customer = new Customer();

    Optional<Address> address = addressRepository.findById(addCustomer.getAddressId());
    if (address.isPresent()) {
      customer.setAddress(address.get());
    } else {
      return null;
    }
    customer.setEmail(addCustomer.getEmail());
    customer.setPhone(addCustomer.getPhone());
    customer.setFirstName(addCustomer.getFirstName());
    customer.setLastName(addCustomer.getLastName());

    logger.info("Adding customer => " + customer);
    return CustomerMapper.mapCustomer(customerRepository.save(customer));
  }
}