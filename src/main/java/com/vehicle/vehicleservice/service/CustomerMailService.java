package com.vehicle.vehicleservice.service;

import java.util.Optional;

import com.vehicle.vehicleservice.api.request.AddCustomerMail;
import com.vehicle.vehicleservice.domain.model.Customer;
import com.vehicle.vehicleservice.domain.model.CustomerMail;
import com.vehicle.vehicleservice.domain.repository.CustomerMailRepository;
import com.vehicle.vehicleservice.domain.repository.CustomerRepository;
import com.vehicle.vehicleservice.dto.CustomerMailDto;
import com.vehicle.vehicleservice.mapper.CustomerMailMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerMailService {
  @Autowired
  private CustomerMailRepository customerMailRepository;
  @Autowired
  private CustomerRepository customerRepository;

  public CustomerMailDto addCustomerMail(AddCustomerMail addCustomerMail) {
    CustomerMail customerMail = new CustomerMail();

    Optional<Customer> customer = customerRepository.findById(addCustomerMail.getCustomerId());
    if (customer.isPresent()) {
      customerMail.setCustomer(customer.get());
    } else {
      return null;
    }

    customerMail.setContactType(addCustomerMail.getContactType());
    customerMail.setContactInfo(addCustomerMail.getContactInfo());
    customerMail.setContactBody(addCustomerMail.getContactBody());
    customerMail.setStatus(addCustomerMail.getStatus());

    return CustomerMailMapper.mapCustomerMail(customerMailRepository.save(customerMail));
  }
}