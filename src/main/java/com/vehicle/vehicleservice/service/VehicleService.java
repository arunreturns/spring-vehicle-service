package com.vehicle.vehicleservice.service;

import java.util.Optional;

import com.vehicle.vehicleservice.api.request.AddVehicle;
import com.vehicle.vehicleservice.domain.model.Customer;
import com.vehicle.vehicleservice.domain.model.Vehicle;
import com.vehicle.vehicleservice.domain.repository.CustomerRepository;
import com.vehicle.vehicleservice.domain.repository.VehicleRepository;
import com.vehicle.vehicleservice.dto.VehicleDto;
import com.vehicle.vehicleservice.mapper.VehicleMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VehicleService {
  @Autowired
  private VehicleRepository vehicleRepository;
  @Autowired
  private CustomerRepository customerRepository;

  public VehicleDto addVehicle(AddVehicle addVehicle) {
    Vehicle vehicle = new Vehicle();

    Optional<Customer> customer = customerRepository.findById(addVehicle.getCustomerId());
    if (customer.isPresent()) {
      vehicle.setCustomer(customer.get());
    } else {
      return null;
    }
    vehicle.setVehicleRegNo(addVehicle.getVehicleRegNo());
    vehicle.setVehicleMake(addVehicle.getVehicleMake());
    vehicle.setVehiclePurchaseDate(addVehicle.getVehiclePurchaseDate());
    vehicle.setHasInsurance(addVehicle.getHasInsurance());

    return VehicleMapper.mapVehicle(vehicleRepository.save(vehicle));
  }
}