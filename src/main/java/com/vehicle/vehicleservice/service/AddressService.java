package com.vehicle.vehicleservice.service;

import com.vehicle.vehicleservice.api.request.AddAddress;
import com.vehicle.vehicleservice.domain.model.Address;
import com.vehicle.vehicleservice.domain.repository.AddressRepository;
import com.vehicle.vehicleservice.dto.AddressDto;
import com.vehicle.vehicleservice.mapper.AddressMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddressService {
  @Autowired
  private AddressRepository addressRepository;

  public AddressDto addAddress(AddAddress addAddress) {
    Address address = new Address();

    address.setAddressLine1(addAddress.getAddressLine1());
    address.setAddressLine2(addAddress.getAddressLine2());
    address.setCity(addAddress.getCity());
    address.setCountry(addAddress.getCountry());
    address.setState(addAddress.getState());
    address.setZip(addAddress.getZip());

    return AddressMapper.mapAddress(addressRepository.save(address));
  }
}