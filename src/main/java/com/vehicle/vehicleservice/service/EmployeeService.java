package com.vehicle.vehicleservice.service;

import java.util.Optional;

import com.vehicle.vehicleservice.api.request.AddEmployee;
import com.vehicle.vehicleservice.domain.model.Address;
import com.vehicle.vehicleservice.domain.model.Employee;
import com.vehicle.vehicleservice.domain.repository.AddressRepository;
import com.vehicle.vehicleservice.domain.repository.EmployeeRepository;
import com.vehicle.vehicleservice.dto.EmployeeDto;
import com.vehicle.vehicleservice.mapper.EmployeeMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {
  @Autowired
  private EmployeeRepository employeeRepository;
  @Autowired
  private AddressRepository addressRepository;

  public EmployeeDto addEmployee(AddEmployee addEmployee) {
    Employee employee = new Employee();

    Optional<Address> address = addressRepository.findById(addEmployee.getAddressId());
    if (address.isPresent()) {
      employee.setAddress(address.get());
    } else {
      return null;
    }
    employee.setEmail(addEmployee.getEmail());
    employee.setPhone(addEmployee.getPhone());
    employee.setFirstName(addEmployee.getFirstName());
    employee.setLastName(addEmployee.getLastName());
    employee.setRole(addEmployee.getRole());

    return EmployeeMapper.mapEmployee(employeeRepository.save(employee));
  }
}